<?php

	require('connection.php');

	if(!empty($_GET['num'])) {
		$id = $_GET['num'];
	} else {
		header('Location: ../index.php');
	}

	try {
		// Istrinti irasa
		$sql = "DELETE FROM prekes WHERE id=$id";

		if($conn->exec($sql)) {
			header('Location: ../index.php');
		}

	} catch(PDOException $e) {
		echo "Negalima istrinti iraso" . $e->getMessage();
	}
