<?php

	require('connection.php');

	if(!empty($_GET['vaisius']) 
		&& !empty($_GET['kiekis']) 
		&& !empty($_GET['kaina'])) {
			$vaisius = $_GET['vaisius'];
			$kiekis = (int)$_GET['kiekis'];
			$kaina = (float)$_GET['kaina'];
	} else {
		header('Location: ../index.php');
	}

	try {

		// Iterpti naujam irasui
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO prekes
		(vaisius, kiekis, kaina)
		VALUES 
		('$vaisius', $kiekis, $kaina)";

		$conn->exec($sql);
		header('Location: ../index.php');

	} catch(PDOException $e) {
		echo "Negalima iterpti iraso" . $e->getMessage();
	}