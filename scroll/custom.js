var upperMiddle = document.getElementById('upper-middle-section');
var lowerMiddle = document.getElementById('lower-middle-section');

var upTop = upperMiddle.offsetTop;
var lowTop = lowerMiddle.offsetTop;

var fixedElemUp = document.getElementById('letsFixedItUp');
var fixedElemBot = document.getElementById('letsFixedItBot');

document.addEventListener('scroll', function() {
	scrollMagic();
});

document.addEventListener('resize', function() {
	scrollMagic();
});

function scrollMagic() {
	var sp = window.pageYOffset;

	if(sp >= upTop) {
		fixedElemUp.classList.add('left-fixed');
	} else {
		fixedElemUp.classList.remove('left-fixed');
	}

	if(sp >= lowTop ) {
		fixedElemBot.classList.add('left-fixed');
	} else {
		fixedElemBot.classList.remove('left-fixed');
	}
}