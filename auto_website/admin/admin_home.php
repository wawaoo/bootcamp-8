<?php
	
	require('../db/connection.php');

	// $stmt = $conn->prepare("SELECT * FROM cars");
	// $stmt->execute();

	// $result = $stmt->fetchAll();

	// $conn = null;
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin paskyra</title>
	
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="../dist/css/main.min.css">
</head>
<body>
	
	<div class="container-fluid page">
		<!-- Išspausdinti admin vardą -->
		<!-- Rodyti lentelę automobilių su veiksmais -->
	
		<div class="container">
			<h1>Automobilių sąrašas:</h1>

		<a href="cars/create.php" class="btn btn-success">Įterpti naują automobilį</a>

		<table class="table table-hover">
			<tr>
				<th>#</th>
				<th>Markė</th>
				<th>Modelis</th>
				<th>Valst. nr.</th>
				<th>Veiksmas</th>
			</tr>
			<?php
				
				$counter = 1;
				
				foreach ($result as $row) :?>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<a href="cars/update.php?num=<?php echo $row['id']; ?>" 
								class="btn btn-warning">
								Atnaujinti
							</a>
							<a href="cars/delete.php?num=<?php echo $row['id']; ?>" 
								class="btn btn-danger">
								Trinti
							</a>
						</td>
					</tr>
				<?php
				endforeach;
			?>
		</table>
		</div>
	</div>

	
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	<script src="dist/js/app.min.js"></script>
</body>
</html>