$(document).ready(function() {
	$('#resetRace').hide();
	var trackLength = 500;


	function calcRacerTime(weight, age) {
		return (trackLength/(weight * age)) * (Math.random() * 0.9 + Math.random()) * 50;
	}

	function random(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	var racer = function(name, weight, age) {
		this.name = name;
		this.weight = weight;
		this.age = age;
		this.time = parseFloat(calcRacerTime(this.weight, this.age).toFixed(3));
	}

	var racersArr = [];

	var batman = new racer('Batman', random(70, 100), random(25, 75));
	racersArr.push(batman);

	var human = new racer('Human', random(50, 100), random(25, 75));
	racersArr.push(human);

	var nindze = new racer('Nindze', random(50, 100), random(15, 60));
	racersArr.push(nindze);

	$('#startRace').on('click', function() {
		var batmanRacer = $('.batman');
		var humanRacer = $('.human');
		var nindzeRacer = $('.nindze');

		batmanRacer.css({
			'transition': batman.time + 's',
			'left': window.innerWidth - 350 + 'px'
		});

		humanRacer.css({
			'transition': human.time + 's',
			'left': window.innerWidth - 350 + 'px'
		});

		nindzeRacer.css({
			'transition': nindze.time + 's',
			'left': window.innerWidth - 350 + 'px'
		});

		setTimeout(function() {
			var num = findWinner();
			$('#winner').text("Nugaletojas: " + racersArr[num].name + ": " + racersArr[num].time);
			
			$('#list').append('<li>' + batman.name + ': ' + batman.time + '</li>');
			$('#list').append('<li>' + human.name + ': ' + human.time + '</li>');
			$('#list').append('<li>' + nindze.name + ': ' + nindze.time + '</li>');


		}, findLooser());
	
	});

	function findWinner() {

		var winner = 0;
		var bestTime = racersArr[winner].time;

		for(var i = 1; i < racersArr.length; i++) {
			if(racersArr[i].time < bestTime) {
				winner = i;
				bestTime = racersArr[i].time;
			}
		}

		return winner;
	}

	function findLooser() {
		var looser = 0;
		var worstTime = racersArr[looser].time;

		for(var i = 1; i < racersArr.length; i++) {
			if(racersArr[i].time > worstTime) {
				looser = i;
				worstTime = racersArr[i].time;
			}
		}

		return worstTime * 1000;
	}

	$('#resetRace').on('click', function() {
		location.href = 'index.html';
	});

});
